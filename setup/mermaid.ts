import { defineMermaidSetup } from '@slidev/types'

export default defineMermaidSetup(() => {
    return {
        theme: 'forest',
        gantt: {
            titleTopMargin: 25,
            barHeight: 100,
            barGap: 4,
            topPadding: 75,
            sidePadding: 75,
            fontSize: 36,
            sectionFontSize: 36,
            leftPadding: 200,
        }
    }
})