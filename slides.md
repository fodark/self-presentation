---
# try also 'default' to start simple
theme: unicorn
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
drawings:
  persist: false
layout: intro
introImage: '/self-presentation/face.png'
logoHeader: '/self-presentation/out.png'
website: 'fodark.xyz'
handle: 'fodark'
---
# Nicola Dall'Asen
## AI PhD Student @UniPi/UniTn
### Self presentation

---
layout: default
---
# My Path

```mermaid
gantt
    title Academic path
    dateFormat  YYYY-MM-DD
    section Bachelor's
    Computer Science           :done, 2016-09-01, 2019-07-20
    section Master's
    Data Science      :done, 2019-09-01, 2021-10-22
    section PhD
    AI      :active, crit, 2021-11-01, 2024-11-01
```


<style>
  .mermaid {
    margin-top: -100px;
  }
</style>
---
layout: default
---
# Research Topic

### Formally: "Social Robotics for elderly assistance"
#### Supervisor: Elisa Ricci

<br>
<hr>
<br>

### Practically: Anonymisation through GANs to preserve privacy
#### Ultimate goal: Temporal-coherent analyses-preserving anonymisation framework
---
layout: default
---
# Previous Work(hopefully I can an 's' soon)

## AnonyGAN - ICIAP 2021

![anonygan](/teaser.png)

- Based on Hao's BiGraphGAN[^1] bipartite graph formulation
  - Perform geometric reasoning among face landmarks to preserve pose
- Outperforms SOTA in terms of visual naturaleness and pose preservations, still lags in terms of anonymisation

[^1]: Tang H. et al., Bipartite Graph Reasoning GANs for Person Image Generation, 2020

<style>
  img {
    max-width: 66%;
    margin-left: auto;
    margin-right: auto;
  }
  .footnotes {
    margin-top: -60px !important;
  }
</style>
---
layout: default
---
# From Now On (chasing ECCV)

- Perform face swapping in low-resolution, where faces are still recognisable but SOTA fails
  - Curriculum learning on resolution, performing face swap in high-resolution is easier

- In the end, adaptive face swapping, therefore not limited to particular resolution